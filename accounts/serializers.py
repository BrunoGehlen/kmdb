from rest_framework import serializers

from django.contrib.auth.models import User


class UserSerializer(serializers.ModelSerializer):
    class Meta:
        model = User
        fields = "__all__"
        extra_kwargs = {
            "password": {"write_only": True},
            "last_login": {"write_only": True},
            "email": {"write_only": True},
            "date_joined": {"write_only": True},
            "groups": {"write_only": True},
            "user_permissions": {"write_only": True},
            "is_active": {"write_only": True},
        }

    def create(self, validated_data):
        return User.objects.create_user(**validated_data)


class CredentialSerialzier(serializers.Serializer):
    username = serializers.CharField()
    password = serializers.CharField()
