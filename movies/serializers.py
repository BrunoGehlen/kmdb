from rest_framework import serializers
from .models import Movie, Genre, Comment, Review
from accounts.serializers import UserSerializer
from django.contrib.auth.models import User


class CommentSerializer(serializers.ModelSerializer):
    class Meta:
        model = Comment
        fields = ["comment", "user", "movie"]

    def create(self, validated_data, *args, **kwargs):
        return Comment.objects.create(**validated_data)


class CommentUpdateSerializer(serializers.ModelSerializer):
    class Meta:
        model = Comment
        fields = ["comment", "user", "movie"]
        extra_kwargs = {"user": {"read_only": True}, "movie": {"read_only": True}}


class CommentUserOutputSerializer(serializers.ModelSerializer):
    class Meta:
        model = User
        fields = ["id", "first_name", "last_name"]


class CommentOutputSerializer(serializers.ModelSerializer):
    user = CommentUserOutputSerializer()

    class Meta:
        model = Comment
        fields = ["id", "comment", "user"]


class GenreSerializer(serializers.ModelSerializer):
    class Meta:
        model = Genre
        fields = ["id", "name"]


class CriticReviewSerialzer(serializers.ModelSerializer):
    class Meta:
        model = User
        fields = ["id", "first_name", "last_name"]


class ReviewOutputSerializer(serializers.ModelSerializer):
    critic = CriticReviewSerialzer()

    class Meta:
        model = Review
        fields = ["id", "critic", "stars", "review", "spoilers"]


class MovieSerializer(serializers.ModelSerializer):
    genres = GenreSerializer(many=True)
    comment_set = CommentOutputSerializer(many=True, read_only=True)
    criticism_set = ReviewOutputSerializer(many=True, read_only=True)

    class Meta:
        model = Movie
        fields = [
            "id",
            "title",
            "duration",
            "genres",
            "classification",
            "launch",
            "synopsis",
            "criticism_set",
            "comment_set",
        ]
        depth = 1

    def create(self, validated_data):
        genres = validated_data.pop("genres")
        movie = Movie.objects.get_or_create(**validated_data)[0]

        for genre in genres:
            new_genre = Genre.objects.get_or_create(name=genre["name"])[0]
            movie.genres.add(new_genre)
        return movie


class ReviewSerializer(serializers.ModelSerializer):
    class Meta:
        model = Review
        fields = ["id", "review", "spoilers", "critic", "stars", "movie"]


class ReviewUpdateSerializer(serializers.ModelSerializer):
    critic = CriticReviewSerialzer(read_only=True)

    class Meta:
        model = Review
        fields = ["id", "review", "spoilers", "stars", "critic"]
