from django.db import models
from django.contrib.auth.models import User
from django.core.validators import MaxValueValidator, MinValueValidator


class Genre(models.Model):
    name = models.CharField(max_length=255, default="No name given")


class Movie(models.Model):
    title = models.CharField(max_length=255, default="No title")
    duration = models.CharField(max_length=255, default="No duration")
    genres = models.ManyToManyField(Genre, related_name="movies")
    launch = models.DateField()
    classification = models.IntegerField()
    synopsis = models.TextField()


class Comment(models.Model):
    comment = models.CharField(max_length=255, default="No content")
    user = models.ForeignKey(User, related_name="comment_set", on_delete=models.CASCADE)
    movie = models.ForeignKey(
        Movie, related_name="comment_set", on_delete=models.CASCADE
    )


class Review(models.Model):
    review = models.TextField()
    spoilers = models.BooleanField(default=False)
    stars = models.IntegerField(
        default=0, validators=[MaxValueValidator(10), MinValueValidator(0)]
    )
    critic = models.ForeignKey(User, on_delete=models.CASCADE)
    movie = models.ForeignKey(
        Movie, related_name="criticism_set", on_delete=models.CASCADE
    )
