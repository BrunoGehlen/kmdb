from rest_framework.permissions import BasePermission
from rest_framework.exceptions import ValidationError
from rest_framework.response import Response
from rest_framework import status


class MoviePermission(BasePermission):
    def has_permission(self, request, view):
        if request.method == "POST" and request.user.is_superuser:
            return True

        if request.method == "GET":
            return True

        if request.method == "DELETE" and request.user.is_superuser:
            return True

        return False


class CommentPermission(BasePermission):
    def has_permission(self, request, view):
        if (
            request.method == "POST"
            and request.user.is_superuser
            or request.user.is_staff
        ):
            return False

        else:
            return True


class ReviewPermission(BasePermission):
    def has_permission(self, request, view):
        if request.method == "POST" and request.user.is_superuser:
            return False

        if request.method == "POST" and request.user.is_staff:
            return True
        if request.method == "PUT" and request.user.is_superuser:
            return False
        if request.method == "PUT" and request.user.is_staff:
            return True

        return False
