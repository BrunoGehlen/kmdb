from .views import CreateMovieView, CreateCommentView, DeleteMovieView, MovieReview
from django.urls import path

urlpatterns = [
    path("movies/", CreateMovieView.as_view()),
    path("movies/<int:pk>/comments/", CreateCommentView.as_view()),
    path("movies/<int:pk>/", DeleteMovieView.as_view()),
    path("movies/<int:pk>/review/", MovieReview.as_view()),
]


# from rest_framework.routers import DefaultRouter

# router = DefaultRouter()
# router.register(r'student-viewset', StudentViewSet,)
# router.register(r'student-generic-viewset', StudentGenericViewSet)
# router.register(r'student-model-viewset', StudentGenericViewSet)
# urlpatterns = router.urls
