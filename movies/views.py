from rest_framework.generics import (
    ListCreateAPIView,
    CreateAPIView,
    DestroyAPIView,
    RetrieveDestroyAPIView,
    UpdateAPIView,
)
from .models import Movie, Comment, Review
from .serializers import (
    MovieSerializer,
    CommentSerializer,
    CommentOutputSerializer,
    ReviewSerializer,
    ReviewOutputSerializer,
    ReviewUpdateSerializer,
    CommentUpdateSerializer,
)
from .permissions import MoviePermission, CommentPermission, ReviewPermission
from rest_framework.authentication import TokenAuthentication
from rest_framework.permissions import IsAuthenticatedOrReadOnly
from rest_framework.mixins import (
    ListModelMixin,
    CreateModelMixin,
    UpdateModelMixin,
)
from rest_framework.generics import GenericAPIView
from rest_framework.response import Response
from rest_framework import status
from django.core.exceptions import ObjectDoesNotExist


class MultipleFieldLookupMixin:
    def get_queryset(self):
        queryset = self.queryset
        lookup_filter = {}
        for lookup_field in self.lookup_fields:
            if self.request.data.get(lookup_field):
                lookup_filter[f"{lookup_field}__icontains"] = self.request.data.get(
                    lookup_field
                )
        queryset = queryset.filter(**lookup_filter)
        return queryset


class DeleteMovieView(RetrieveDestroyAPIView):
    authentication_classes = [TokenAuthentication]
    permission_classes = [MoviePermission, IsAuthenticatedOrReadOnly]
    queryset = Movie.objects.all()
    serializer_class = MovieSerializer


class MovieReview(CreateAPIView, UpdateAPIView):
    authentication_classes = [TokenAuthentication]
    permission_classes = [ReviewPermission]
    queryset = Review
    serializer_class = ReviewSerializer

    def post(self, request, *args, **kwargs):
        return self.create(request, *args, **kwargs)

    def create(self, request, *args, **kwargs):
        request.data["critic"] = request.user.id
        try:
            movie = Movie.objects.get(pk=kwargs["pk"])
            for criticism in movie.criticism_set.all():
                if request.user.id == criticism.__dict__["critic_id"]:
                    return Response(
                        {"detail": "You already made this review."},
                        status=status.HTTP_422_UNPROCESSABLE_ENTITY,
                    )
        except Movie.DoesNotExist:
            return Response({"detail": "Not found."}, status=404)

        request.data["movie"] = movie.id
        serializer = self.get_serializer(data=request.data)
        serializer.is_valid(raise_exception=True)
        review = serializer.save()
        serializer = ReviewOutputSerializer(review)
        headers = self.get_success_headers(serializer.data)
        return Response(
            serializer.data, status=status.HTTP_201_CREATED, headers=headers
        )

    def update(self, request, *args, **kwargs):

        partial = kwargs.pop("partial", False)
        instance = self.get_object()
        serializer = ReviewUpdateSerializer(
            instance, data=request.data, partial=partial
        )
        serializer.is_valid(raise_exception=True)
        self.perform_update(serializer)

        movie = Movie.objects.get(id=kwargs["pk"])
        try:
            review = Review.objects.get(movie=movie, critic=request.user)

        except Review.DoesNotExist:
            return Response({"detail": "Not found."}, status=status.HTTP_404_NOT_FOUND)

        review.stars = request.data["stars"]
        review.review = request.data["review"]
        review.spoilers = request.data["spoilers"]
        review.save()

        serializer = ReviewUpdateSerializer(review)

        if getattr(instance, "_prefetched_objects_cache", None):
            # If 'prefetch_related' has been applied to a queryset, we need to
            # forcibly invalidate the prefetch cache on the instance.
            instance._prefetched_objects_cache = {}

        return Response(serializer.data)


class CreateMovieView(MultipleFieldLookupMixin, ListCreateAPIView):
    authentication_classes = [TokenAuthentication]
    permission_classes = [MoviePermission, IsAuthenticatedOrReadOnly]
    queryset = Movie.objects.all()
    serializer_class = MovieSerializer
    lookup_fields = ["title"]


# /api/movies/1/comments/
class CreateCommentView(
    GenericAPIView, ListModelMixin, CreateModelMixin, UpdateModelMixin
):
    queryset = Comment.objects.all()
    serializer_class = CommentSerializer
    authentication_classes = [TokenAuthentication]
    permission_classes = [CommentPermission]

    def post(self, request, *args, **kwargs):
        return self.create(request, *args, **kwargs)

    def create(self, request, *args, **kwargs):
        request.data["user"] = request.user.id

        try:
            movie = Movie.objects.get(pk=kwargs["pk"]).id

        except Movie.DoesNotExist:
            return Response({"detail": "Not found."}, status=404)

        request.data["movie"] = movie
        serializer = self.get_serializer(data=request.data)
        serializer.is_valid(raise_exception=True)

        comment = serializer.save()
        serializer = CommentOutputSerializer(comment)
        headers = self.get_success_headers(serializer.data)
        return Response(
            serializer.data, status=status.HTTP_201_CREATED, headers=headers
        )

    def put(self, request, *args, **kwargs):
        return self.update(request, *args, **kwargs)

    def update(self, request, *args, **kwargs):

        partial = kwargs.pop("partial", False)
        instance = self.get_object()
        serializer = CommentUpdateSerializer(
            instance, data=request.data, partial=partial
        )
        serializer.is_valid(raise_exception=True)
        self.perform_update(serializer)
        movie = Movie.objects.get(id=kwargs["pk"])
        try:
            comment = Comment.objects.get(movie=movie, user=request.user)

        except Review.DoesNotExist:
            return Response({"detail": "Not found."}, status=status.HTTP_404_NOT_FOUND)

        if comment.id == request.data["comment_id"]:
            comment.comment = request.data["comment"]
            comment.save()
        else:
            return Response({"detail": "Not found."}, status=status.HTTP_404_NOT_FOUND)

        serializer = CommentSerializer(comment)

        if getattr(instance, "_prefetched_objects_cache", None):
            instance._prefetched_objects_cache = {}

        return Response(serializer.data)
